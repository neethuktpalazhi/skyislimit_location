import 'package:flutter/material.dart';
import 'package:skyislimit_location/ui/homeScreen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ICOTravel',
      theme: ThemeData(
        primarySwatch: Colors.yellow,
        //fontFamily: 'Lobster Two'
      ),
      debugShowCheckedModeBanner: false,
      home: HomeScreen(title: 'ICOTravel'),
    );
  }
}
